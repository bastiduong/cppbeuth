#include <iostream>
#include <vector>
#include "TDate.h"
#include "TMoney.h"
#include "TTime.h"
#include "TAddress.h"
#include "TMember.h"
#include "TSportsclub.h"
#include "TEvent.h"
#include "TFoul.h"
#include "TGoal.h"
#include "TSubstitution.h"
#include "TPenalty.h"
#include "TLeague.h"
#include "TMatch.h"
#include "TDateTime.h"
#include "TLocation.h"

using namespace std;

ostream &DoppelterZeilenumbruch(ostream &ostr)
{
	ostr << endl << endl;
	return ostr;
}

int main() {
	//Uebung 1

	/*
	TDate D1, D2(03, 10, 2016);
	TMoney Amount1, Amount2(150.0), Amount3(19.9, "SFr");

	cout << "Klasse TDate:" << endl;
	cout << "Standardkonstruktor Heutiges Datum (D1): ";   D1.printDate();   cout << endl;
	cout << "Konstruktor Tag der dt. Einheit (D2): ";   D2.printDate();   cout << endl;

	cout << "\nKlasse TMoney:" << endl;
	cout << "Betrag 1: "; Amount1.print(); cout << endl;
	cout << "Betrag 2: "; Amount2.print(); cout << endl;
	cout << "Betrag 3: "; Amount3.print(); cout << endl;
	*/

	//Uebung 2
	/*
	TTime T1, T2(14, 15);
	TAddress Hochschule("Luxemburger Str. 10", "12353", "Berlin");
	TMember Mitglied("Max Mustermann", 24, 6, 1988, "Mustergassse 3B", "91919", "Musterdorf", 4711);

	cout << "Klasse TTime:" << endl;
	cout << "Zeit 1: "; T1.print(); cout << endl;
	cout << "Zeit 2: "; T2.print(); cout << endl;

	cout << "\nKlasse TAddress:" << endl;
	cout << "Adresse der Hochschule: "; Hochschule.print(); cout << endl;

	cout << "\nKlasse TMember:" << endl;
	Mitglied.print();
	cout << endl;
	*/
	//Uebung 3
	/*
	TMember Fussballtrainer("Fritz Froehlich", 24, 6, 1968,"Friedhofgasse 3b", "97979", "Freudorf", 4711);
	TMember Tischtennistrainer("Tim Tost", 7, 7, 1977,"Talerweg 27", "97977", "Tortenheim", 815);
	TTeam Torlos("1. FC Torlos", &Fussballtrainer);
	TTeam Kantenball("TTC Kantenball", &Tischtennistrainer);
	TSportsclub TSVBHT("TSV BHT", 7, 10, 2016);
	TPlayer Mitglied1("Anton Albrecht", 1, 1, 1981,"Antoniusstr. 5", "97979", "Freudorf", 1, 17, "Torwart");
	TPlayer Mitglied2("Bernd Baller", 2, 2, 1982,"Badstr. 34", "97978", "Sonnental", 2, 5, "Abwehrspieler");
	TPlayer Mitglied3("Caesar Caspar", 3, 3, 1983,"Cellweg 12", "97979", "Freudorf", 3, 8, "Mittelfeld");
	TPlayer Mitglied4("Detlef Dropke", 4, 4, 1984,"Dorfpfad 7", "97977", "Tortenheim", 4, 11, "Stuermer");
	TPlayer Mitglied5("Emil Einfalt", 5, 5, 1985,"Einheitsstr. 19", "97978", "Sonnental", 5, 1, "Einzel 1");
	TPlayer Mitglied6("Gustav Ganz", 6, 6, 1986,"Gerader Weg 22", "97979", "Freudorf", 6, 2, "Einzel 2");
	TPlayer Mitglied7("Heinrich Hastig", 7, 7, 1987,"Halbstr. 1a", "97977", "Tortenheim", 7, 3, "Einzel 3");
	TPlayer Mitglied8("Igor Interferenz", 8, 8, 1988,"Immentaler Platz 22", "97979", "Freudorf", 8, 4, "Einzel 4");

	TSVBHT.addTeam(&Torlos);
	TSVBHT.addTeam(&Kantenball);

	TSVBHT.addMember(&Mitglied1, "1. FC Torlos");
	TSVBHT.addMember(&Mitglied2, "1. FC Torlos");
	TSVBHT.addMember(&Mitglied3, "1. FC Torlos");
	TSVBHT.addMember(&Mitglied4, "1. FC Torlos");
	TSVBHT.addMember(&Mitglied5, "TTC Kantenball");
	TSVBHT.addMember(&Mitglied6, "TTC Kantenball");
	TSVBHT.addMember(&Mitglied7, "TTC Kantenball");
	TSVBHT.addMember(&Mitglied8, "TTC Kantenball");

	TSVBHT.printMembers();
	cout << endl;
	TSVBHT.printTeams();
	cout << endl;

	getchar();
	return 0;

	*/

	//Uebung 4
	/* 
		TSportsclub TSVBHT("Sportsclub.database");

		TSVBHT.printMembers();
		cout << endl;
		TSVBHT.printTeams();

		getchar();
		return 0;
	*/

	/*//Uebung 5
	TDate Spieltag(25, 11, 2016);
	TSportsclub TSVBHT("Sportsclub.database");
	
	vector <TEvent *> Events;
	unsigned i = 0;
	Events.push_back(new TFoul(14, TSVBHT.getTeam(0).getPlayer(1), TSVBHT.getTeam(1).getPlayer(2), TCard::yellow));
	Events.push_back(new TGoal(27, TSVBHT.getTeam(0).getPlayer(0), false));
	Events.push_back(new TSubstitution(39, TSVBHT.getTeam(0).getPlayer(1),
		TSVBHT.getTeam(0).getPlayer(3)));
	Events.push_back(new TFoul(51, TSVBHT.getTeam(0).getPlayer(3),
		TSVBHT.getTeam(1).getPlayer(1), TCard::red));
	Events.push_back(new TGoal(63, TSVBHT.getTeam(1).getPlayer(1), true));
	Events.push_back(new TPenalty(83, TSVBHT.getTeam(1).getPlayer(2), true));


	cout << "Spiel zwischen "
		<< TSVBHT.getTeam(0).getTeamname() << " und "
		<< TSVBHT.getTeam(1).getTeamname() << " am " << Spieltag << ":" << DoppelterZeilenumbruch;
	for (i = 0; i < Events.size(); i++) {
		cout << *(Events.at(i));
	}
	
	getchar();
	*/

	//�bung 6
	TLeague HochschulLiga("League.database");
	HochschulLiga.print();
	return 0;
}