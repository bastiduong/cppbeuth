#ifndef TGOAL_H
#define TGOAL_H


#include "TMatch.h"
#include "TEvent.h"
class TGoal :public TEvent
{
private:
	bool owngoal;
	bool shotByHome = true; // true = home; false = guest
public:
	TGoal(int minute, TMember *member, bool owngoalornot ) {
		setGameMinute(minute);
		setMember(member);
		owngoal = owngoalornot;
	};
	TGoal() {
		owngoal = false;
	};
	~TGoal();
	void load(ifstream &data, TTeam *home, TTeam *guest, TMatch *match);
	void print();
};

#endif