#include "TGoal.h"


TGoal::~TGoal()
{
}

void TGoal::load(ifstream & data, TTeam * home, TTeam * guest, TMatch * match)
{
	string line = "";
	while (getline(data, line)) {
		if (line.find("<MinuteOfPlay>") != -1) {
			string StartPattern = "<MinuteOfPlay>";
			string EndPattern = "</MinuteOfPlay>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string str(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			setGameMinute(atoi(str.c_str()));
		}

		if (line.find("<PlayerTricotNr Team=\"Guest\">") != -1) {
			string StartPattern = "<PlayerTricotNr Team=\"Guest\">";
			string EndPattern = "</PlayerTricotNr>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string str(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			int tricotnumber = atoi(str.c_str());

			shotByHome = false;

			for (int i = 0; i < guest->getNumberOfPlayerInTeam(); i++) {

				if (guest->getPlayer(i)->getTrikotnumber() == tricotnumber) {

					setMember(guest->getPlayer(i));
				}
			}
		}

		if (line.find("<PlayerTricotNr Team=\"Home\">") != -1) {
			string StartPattern = "<PlayerTricotNr Team=\"Home\">";
			string EndPattern = "</PlayerTricotNr>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string str(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			int tricotnumber = atoi(str.c_str());

			shotByHome = true;

			for (int i = 0; i < home->getNumberOfPlayerInTeam(); i++) {

				if (home->getPlayer(i)->getTrikotnumber() == tricotnumber) {
					setMember(home->getPlayer(i));
				}
			}
		}

		if (line.find("<OwnGoal>") != -1) {
			string StartPattern = "<OwnGoal>";
			string EndPattern = "</OwnGoal>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string str(line.begin() + StartPos + sizePattern, line.begin() + EndPos);

			if (str == "true") {
				owngoal = true;
			}
		}

		if (line.find("</Goal>") != -1) {
			if (!owngoal) {
				if (shotByHome) {
					match->incHomePoints();
				}
				else {
					match->incGuestPoints();
				}
			}
			else {
				if (shotByHome) {
					match->incGuestPoints();
				}
				else {
					match->incHomePoints();
				}
			}
			break;
		}
	}
}


void TGoal::print()
{
	cout << this->getGameMinute() << ". Spielminute: " << ((owngoal) ? "EIGENTOR" : "TOR!!!") << endl;
	cout << "Torschuetze " << this->getMember().getName() << " (" << this->getMember().getTrikotnumber() << ")" << endl << endl;

}
