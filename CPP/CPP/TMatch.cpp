#include "TMatch.h"
#include "TPenalty.h"
#include "TLocation.h"
#include "TDate.h"
#include "TLeague.h"
#include "TFoul.h"
#include "TGoal.h"
#include "TSubstitution.h"


TMatch::TMatch()
{
}


TMatch::~TMatch()
{
}

void TMatch::load(ifstream &data, TLeague * league) {
	
	string line="";
	while (getline(data, line)) {
	
		if (line.find("<DateTime>") != -1) {
			TDateTime *_dtime = new TDateTime();
			_dtime->load(data);
			this->start = *_dtime;
		}
		if (line.find("<Location>") != -1) {
			TLocation *_location = new TLocation();
			_location->load(data);
			this->location = *_location;
		}
		if (line.find("<HomeTeam>") != -1) {
			string StartPattern = "<HomeTeam>";
			string EndPattern = "</HomeTeam>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string str(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			home = league->getTeamByName(str);
		}
		if (line.find("<GuestTeam>") != -1) {
			string StartPattern = "<GuestTeam>";
			string EndPattern = "</GuestTeam>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string str(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			guest = league->getTeamByName(str);
		}
		if (line.find("<Foul>") != -1) {
			TFoul *_event = new TFoul();
			_event->load(data, home, guest);
			gameEvents.push_back(_event);
		}

		if (line.find("<Goal>") != -1) {
			TGoal *_event = new TGoal();
			_event->load(data, home, guest, this);
			gameEvents.push_back(_event);
		}

		if (line.find("<Penalty>") != -1) {
			TPenalty *_event = new TPenalty();
			_event->load(data, home, guest, this);
			gameEvents.push_back(_event);
		}

		if (line.find("<Substitution>") != -1) {
			TSubstitution *_event = new TSubstitution();
			_event->load(data, home, guest);
			gameEvents.push_back(_event);
		}

		if (line.find("</TMatch>") != -1) {
			break;
		}
	}
}

void TMatch::print() {
	printf("Heim : %d Gast : %d\n", homePoints, guestPoints);
}

void TMatch::printEvents()
{
	TList<TEvent*>::Iterator ListIter(this->gameEvents);

	while (ListIter != this->gameEvents.end()) {
		cout << *(*ListIter);
		ListIter++;
	}
}
