#ifndef TTEAM_H
#define TTEAM_H

#include <fstream>
#include <iostream>
#include <strstream>
#include <string>
#include "TPlayer.h"
#define MAXPLAYERINTEAM 50
class TTeam
{
private:
	int currentPlayerInThisTeam = 0;
	string teamname;
	TMember trainer;
	TMember *listOfMemberOfTeam[MAXPLAYERINTEAM];
public:
	TTeam() {};
	TTeam(string teamname, TMember *ptr_trainer) :
		teamname(teamname), trainer(*ptr_trainer) {
		currentPlayerInThisTeam = 0;
	};
	~TTeam() {};
	

	TMember *getListofMemberOfTeam(int index) {
		return listOfMemberOfTeam[index];
	}
	string getTeamname() { return teamname; }
	TMember getTrainer() { return trainer; }
	void addMemberToTeam(TMember *player);
	int getNumberOfPlayerInTeam() { return currentPlayerInThisTeam; }
	void inc() { currentPlayerInThisTeam++; }
	void load(ifstream &data);
	TMember *getPlayer(int index) {
		return listOfMemberOfTeam[index];
	}
};

#endif

