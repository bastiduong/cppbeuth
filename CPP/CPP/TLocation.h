#ifndef TLOCATION_H
#define TLOCATION_H

#include "TAddress.h"

class TLocation : public TAddress
{
private:
	string  Stadium;
	int seats;
public:
	TLocation();
	~TLocation();
	void load(ifstream &data);
};
#endif

