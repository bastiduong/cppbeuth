#ifndef TSUBSTITUTION_H
#define TSUBSTITUTION_H

#include "TMember.h"
#include "TEvent.h"
class TSubstitution : public TEvent
{
private:
	TMember *newplayer = nullptr;
public:
	TSubstitution(int minute, TMember *member1, TMember *member2) {
		setGameMinute(minute);
		setMember(member1);
		newplayer = member2;
	};
	TSubstitution() {};
	~TSubstitution();
	void print();
	void load(ifstream &filename, TTeam * home, TTeam * guest);
};

#endif
