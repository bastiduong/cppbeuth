#ifndef TPENALTY_H
#define TPENALTY_H


#include "TEvent.h"
#include "TMatch.h"
#include "TTeam.h"
class TPenalty :public TEvent
{
private:
	bool penaltyGoal = false;
	bool shotByHome = true; // true = home; false = guest
public:
	TPenalty();
	TPenalty(int minute, TMember * member, bool goal) {
		setGameMinute(minute);
		setMember(member);
		this->penaltyGoal = goal;
	};
	~TPenalty();
	void print();
	void load(ifstream &data, TTeam * home, TTeam * guest, TMatch * match);
};

#endif

