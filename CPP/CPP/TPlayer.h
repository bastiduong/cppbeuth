
#ifndef TPLAYER_H
#define TPLAYER_H


#include "TMember.h"
#include <string>
class TPlayer : public TMember
{
private:

	string position;

public:
	TPlayer() {};
	TPlayer(string name, int day, int month, int year, string street, string plz, string city, short playerid, short trikotnumber, string position) :
		TMember(name, day, month, year, street, plz, city, playerid, trikotnumber), position(position) {};
	~TPlayer() {}
	string getPosition() { return position; }
	void load(ifstream &data);

};

#endif