#include "TSubstitution.h"


TSubstitution::~TSubstitution()
{
}


void TSubstitution::print() {

	cout << this->getGameMinute() << ". Spielminute: WECHSEL!" << endl;
	cout << "Fuer " << this->getMember().getName() << " (" << this->getMember().getTrikotnumber() << ") ";
	cout << "kommt " << this->newplayer->getName() << " (" << this->newplayer->getTrikotnumber() << ")" << endl;
	cout << endl;
}

void TSubstitution::load(ifstream & filename, TTeam * home, TTeam * guest)
{
	string line = "";
	while (getline(filename, line)) {
		if (line.find("<MinuteOfPlay>") != -1) {
			string StartPattern = "<MinuteOfPlay>";
			string EndPattern = "</MinuteOfPlay>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string str(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			setGameMinute(atoi(str.c_str()));
		}

		if (line.find("<PlayerTricotNr Team=\"Guest\">") != -1) {
			string StartPattern = "<PlayerTricotNr Team=\"Guest\">";
			string EndPattern = "</PlayerTricotNr>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string str(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			int tricotnumber = atoi(str.c_str());

			for (int i = 0; i < guest->getNumberOfPlayerInTeam(); i++) {

				if (guest->getPlayer(i)->getTrikotnumber() == tricotnumber) {

					setMember(guest->getPlayer(i));
				}
			}
		}

		if (line.find("<NewPlayerTricotNr Team=\"Guest\">") != -1) {
			string StartPattern = "<NewPlayerTricotNr Team=\"Guest\">";
			string EndPattern = "</NewPlayerTricotNr>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string str(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			int tricotnumber = atoi(str.c_str());

			for (int i = 0; i < guest->getNumberOfPlayerInTeam(); i++) {

				if (guest->getPlayer(i)->getTrikotnumber() == tricotnumber) {

					this->newplayer = guest->getPlayer(i);
				}
			}
		}

		if (line.find("<PlayerTricotNr Team=\"Home\">") != -1) {
			string StartPattern = "<PlayerTricotNr Team=\"Home\">";
			string EndPattern = "</PlayerTricotNr>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string str(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			int tricotnumber = atoi(str.c_str());

			for (int i = 0; i < home->getNumberOfPlayerInTeam(); i++) {

				if (home->getPlayer(i)->getTrikotnumber() == tricotnumber) {
					setMember(home->getPlayer(i));
				}
			}
		}

		if (line.find("<NewPlayerTricotNr Team=\"Home\">") != -1) {
			string StartPattern = "<NewPlayerTricotNr Team=\"Home\">";
			string EndPattern = "</NewPlayerTricotNr>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string str(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			int tricotnumber = atoi(str.c_str());

			for (int i = 0; i < home->getNumberOfPlayerInTeam(); i++) {

				if (home->getPlayer(i)->getTrikotnumber() == tricotnumber) {

					this->newplayer = home->getPlayer(i);
				}
			}
		}

		if (line.find("</Substitution>") != -1) {
			break;
		}
	}
}
