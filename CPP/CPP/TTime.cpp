#define _CRT_SEUCRE_NO_WARNINGS 1
#include "TTime.h"
#include <ctime>
#include <string>
TTime::TTime()
{
	time_t currentTime;
	tm *localTime;
	time(&currentTime);
	localTime = localtime(&currentTime);

	int chour = localTime->tm_hour;
	int cminute = localTime->tm_min;
	int cseconds = localTime->tm_sec;

	if (cseconds == 60) {
		cseconds = 0;
	}

	this->hour = chour;
	this->minute = cminute;
	this->seconds = cseconds;
}

void TTime::setTime(int hour, int minute, int seconds)
{
	this->hour = hour;
	this->minute = minute;
	this->seconds = seconds;
}
void TTime::setHour(int hour) {
	this->hour = hour;
}
void TTime::setMin(int minute) {
	this->minute = minute;
}
void TTime::setSec(int seconds) {
	this->seconds = seconds;
}
void TTime::print()
{
	printf("%02d.%02d.%02d", hour, minute, seconds);
}
