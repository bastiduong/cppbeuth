#include "TPenalty.h"


TPenalty::TPenalty()
{
}

TPenalty::~TPenalty()
{
}

void TPenalty::print()
{
	cout << this->getGameMinute() << ". Spielminute: ELFMETER!" << endl;
	cout << "Ausgefuehrt von " << this->getMember().getName() << " (" << this->getMember().getTrikotnumber() << ") ";

	if (penaltyGoal) {
		cout << " => TOR!" << endl;
	}
	else {
		cout << " => KEIN TOR!" << endl;
	}
	cout << endl;
}

void TPenalty::load(ifstream &filename, TTeam * home, TTeam * guest, TMatch * match) {
	string line = "";
	while (getline(filename, line)) {
		if (line.find("<MinuteOfPlay>") != -1) {
			string StartPattern = "<MinuteOfPlay>";
			string EndPattern = "</MinuteOfPlay>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string str(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			setGameMinute(atoi(str.c_str()));
		}

		if (line.find("<PlayerTricotNr Team=\"Guest\">") != -1) {
			string StartPattern = "<PlayerTricotNr Team=\"Guest\">";
			string EndPattern = "</PlayerTricotNr>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string str(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			int tricotnumber = atoi(str.c_str());

			shotByHome = false;

			for (int i = 0; i < guest->getNumberOfPlayerInTeam(); i++) {

				if (guest->getPlayer(i)->getTrikotnumber() == tricotnumber) {

					setMember(guest->getPlayer(i));
				}
			}
		}

		if (line.find("<PlayerTricotNr Team=\"Home\">") != -1) {
			string StartPattern = "<PlayerTricotNr Team=\"Home\">";
			string EndPattern = "</PlayerTricotNr>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string str(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			int tricotnumber = atoi(str.c_str());

			shotByHome = true;

			for (int i = 0; i < home->getNumberOfPlayerInTeam(); i++) {

				if (home->getPlayer(i)->getTrikotnumber() == tricotnumber) {
					setMember(home->getPlayer(i));
				}
			}
		}

		if (line.find("<Goal>") != -1) {
			string StartPattern = "<Goal>";
			string EndPattern = "</Goal>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string str(line.begin() + StartPos + sizePattern, line.begin() + EndPos);

			if (str == "true") {
				penaltyGoal = true;
			}
		}

		if (line.find("</Penalty>") != -1) {
			if (penaltyGoal) {
				if (shotByHome) {
					match->incHomePoints();
				}
				else {
					match->incGuestPoints();
				}
			}
			break;
		}



	}
}
