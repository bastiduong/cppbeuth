#ifndef TMEMBER_H
#define TMEMBER_H

#include "TDate.h"
#include "TAddress.h"
#include <string>
using namespace std;

class TMember
{
public:
	TMember() { trikotnumber = 0; };
	TMember(string name, int day, int month, int year, string street, string plz, string city, short playerid, short trikotnumber) :
		name(name), birthday(day, month, year), adress(street, plz, city), playerid(playerid), trikotnumber(trikotnumber) {}
	~TMember() {}
	short getPlayerID() { return playerid; }
	void setPlayerID(short playerID) { this->playerid = playerID; }
	string getName() { return name; }
	void setName(string name) { this->name = name; }
	TAddress getAdress() { return adress; }
	void setAddress(TAddress address) { this->adress = adress; }
	TDate getBirthday() { return birthday; }
	void setBirthday(TDate date) { this->birthday = date; }
	void print();
	void load(ifstream &data);
	void setTeam(string _team) { team = _team; }
	string getTeam() { return team; }
	short getTrikotnumber() { return trikotnumber; }
	void setTrikotNumber(short number) { trikotnumber = number; }

private:
	string name;
	TDate birthday;
	short playerid;
	TAddress adress;
	string team;
	short trikotnumber;
};

#endif