#ifndef TSPORTSCLUB_H
#define TSPORTSCLUB_H
#include "TDate.h"
#include "TMember.h"
#include "TTeam.h"
#include "TPlayer.h"
#include <string>
#include <fstream>
#include <iostream>
#include <strstream>

#define MEMBERMAX 200
#define TEAMMAX 200
using namespace std;

class TSportsclub
{
private:
	int currentNumberOfMembers = 0;
	int currentNumberOfTeams = 0;
	string clubname;
	TDate foundingdate;
	TMember *listOfMember[MEMBERMAX];
	TTeam *listOfTeams[TEAMMAX];

public:
	TSportsclub(string name, int day, int month, int year) :
		clubname(name), foundingdate(day, month, year)
	{
		currentNumberOfMembers = 0;
		currentNumberOfTeams = 0;
	};

	TSportsclub();
	~TSportsclub() {}

	string getClubname() { return clubname; }
	TDate getFoundingdate() { return foundingdate; }

	void addTeam(TTeam *team);

	void addMember(TMember *player, string teamname);
	void printTeams();

	void printMembers();
	void load(ifstream &data);
	int getCurrentNumberOfTeams() { return currentNumberOfTeams; }
	TTeam *getTeam(int index) {
		return listOfTeams[index];
	}
};
#endif

