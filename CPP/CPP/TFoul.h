#ifndef TFOUL_H
#define TFOUL_H

#include "TMember.h"
#include "TCard.h"
#include "TEvent.h"
class TFoul :public TEvent
{
private:
	TMember *fouledplayer;
	TCard card;
public:
	TFoul(int minute, TMember *member, TMember *opponent, TCard card) {
		setGameMinute(minute);
		setMember(member);
		fouledplayer = opponent;
		this->card = card;
	};
	TFoul();
	~TFoul();
	void print();
	void load(ifstream &data, TTeam *home, TTeam *guest);
};

#endif
