#ifndef TMONEY_H
#define TMONEY_H
#include <string>
using namespace std;

class TMoney
{
public:
	TMoney();
	TMoney(double value, string currency_parameter = "EUR");

	void SetValue(double money);
	void SetCurrency(string currency);

	double GetValue() { return money; }
	string GetCurrency() { return currency; }

	void print();
private:
	double money;
	string currency;
};



#endif // !TMONEY_H
