#include "TSportsclub.h"

TSportsclub::TSportsclub()
{
}

void TSportsclub::addTeam(TTeam *team)
{
	listOfTeams[currentNumberOfTeams] = team;
	currentNumberOfTeams++;
}

void TSportsclub::addMember(TMember *player, string teamname)
{
	listOfMember[currentNumberOfMembers] = player;
	currentNumberOfMembers++;

	for (int i = 0; i < currentNumberOfTeams; i++) {
		if (listOfTeams[i]->getTeamname() == teamname) {
			listOfTeams[i]->addMemberToTeam(player);
		}
	}
}

void TSportsclub::printTeams()
{


	printf("==================================================\n");
	printf("Verein %s; gegruendet am ", clubname.c_str());
	foundingdate.printDate();
	printf("\n");
	printf("==================================================\n");
	printf("\n");
	printf("Manschaften:\n\n");



	for (int i = 0; i < currentNumberOfTeams; i++) {
		TTeam team = *listOfTeams[i];
		printf("%s \n", team.getTeamname().c_str());
		printf("Trainer: %s  \n", team.getTrainer().getName().c_str());
		printf("MNr.	| Name		        | geb. am    | Trikotnr. \n");
		printf("--------|-----------------------|------------|-----------\n");
		for (int j = 0; j < team.getNumberOfPlayerInTeam(); j++) {
			TMember player = *team.getListofMemberOfTeam(j);

			printf("%-8d|%-23s| %02d.%02d.%02d |%-2d\n",
				player.getPlayerID(),
				player.getName().c_str(),
				player.getBirthday().getDay(),
				player.getBirthday().getMonth(),
				player.getBirthday().getYear(),
				player.getTrikotnumber());
		}
		printf("\n");
	}
}

void TSportsclub::printMembers()
{
	printf("==================================================\n");
	printf("Verein %s; gegruendet am ", clubname.c_str());
	foundingdate.printDate();
	printf("\n");
	printf("==================================================\n");
	printf("\n");
	printf("Mitglieder:\n\n");

	for (int i = 0; i < currentNumberOfTeams; i++) {

		TTeam player = *listOfTeams[i];

		printf("- Mitglied Nr. %d \n", player.getTrainer().getPlayerID());
		printf("%s; geb. %02d.%02d.%02d \n",
			player.getTrainer().getName().c_str(),
			player.getTrainer().getBirthday().getDay(),
			player.getTrainer().getBirthday().getMonth(),
			player.getTrainer().getBirthday().getYear());
		printf("%s; %s %s\n",
			player.getTrainer().getAdress().getStreet().c_str(),
			player.getTrainer().getAdress().getPLZ().c_str(),
			player.getTrainer().getAdress().getCity().c_str());
		printf("\n");
	}

	for (int i = 0; i < currentNumberOfMembers; i++) {

		TMember player = *listOfMember[i];
		printf("- Mitglied Nr. %d \n", player.getPlayerID());
		printf("%s; geb. %02d.%02d.%02d \n", player.getName().c_str(), player.getBirthday().getDay(), player.getBirthday().getMonth(), player.getBirthday().getYear());
		printf("%s; %s %s\n", player.getAdress().getStreet().c_str(), player.getAdress().getPLZ().c_str(), player.getAdress().getCity().c_str());
		printf("\n");
	}



}


void TSportsclub::load(ifstream &data){
	string line;
	while (getline(data, line)){
		if (line.find("<Establishment>") != -1) {
			TDate *_date = new TDate();
			_date->load(data);
			foundingdate = *_date;
		}

		if (line.find("<Name>") != -1) {
			string StartPattern = "<Name>";
			string EndPattern = "</Name>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string name(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			clubname = name.c_str();
		}

		if (line.find("<Team>") != -1) {
			TTeam *_team = new TTeam();
			_team->load(data);
			this->addTeam(_team);

		}

		if (line.find("<Player>") != -1) {
			TMember *_player = new TPlayer();
			_player->load(data);
			this->addMember(_player, _player->getTeam());

		}

		if (line.find("<Member>") != -1) {
			TMember *_member = new TMember();
			_member->load(data);
			this->addMember(_member, _member->getTeam());
		}
		if (line.find("</Sportsclub>") != -1) {
			break;
		}
	}

}

