#include "TFoul.h"
#include <iostream>

TFoul::TFoul() {

}

TFoul::~TFoul()
{
}

void TFoul::print()
{
	cout << this->getGameMinute() << ". Spielminute: FOUL" << endl;
	cout << "an " << fouledplayer->getName() << " (" << fouledplayer->getTrikotnumber() << ")";
	cout << " von " << this->getMember().getName() << " (" << this->getMember().getTrikotnumber() << ") ";

	if (card == TCard::yellow) {
		cout << "(gelbe Karte)" << endl;
	}

	if (card == TCard::red) {
		cout << "(rote Karte)" << endl;
	}

	if (card == TCard::yellow_red) {
		cout << "(gelb-rote Karte)" << endl;
	}
	cout << endl;
}

void TFoul::load(ifstream & data, TTeam *home, TTeam * guest)
{
	string line = "";
	while (getline(data, line)) {
		if (line.find("<MinuteOfPlay>") != -1) {
			string StartPattern = "<MinuteOfPlay>";
			string EndPattern = "</MinuteOfPlay>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string str(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			setGameMinute(atoi(str.c_str()));
		}

		if (line.find("<FouledPlayerTricotNr Team=\"Guest\">") != -1) {
			string StartPattern = "<FouledPlayerTricotNr Team=\"Guest\">";
			string EndPattern = "</FouledPlayerTricotNr>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string str(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			int tricotnumber = atoi(str.c_str());

			for (int i = 0; i < guest->getNumberOfPlayerInTeam(); i++) {

				if (guest->getPlayer(i)->getTrikotnumber() == tricotnumber) {
					this->fouledplayer = guest->getPlayer(i);
				}
			}
		}

		if (line.find("<FouledPlayerTricotNr Team=\"Home\">") != -1) {
			string StartPattern = "<FouledPlayerTricotNr Team=\"Home\">";
			string EndPattern = "</FouledPlayerTricotNr>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string str(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			int tricotnumber = atoi(str.c_str());

			for (int i = 0; i < home->getNumberOfPlayerInTeam(); i++) {

				if (home->getPlayer(i)->getTrikotnumber() == tricotnumber) {
					this->fouledplayer = home->getPlayer(i);
				}
			}
		}

		if (line.find("<PlayerTricotNr Team=\"Home\">") != -1) {
			string StartPattern = "<PlayerTricotNr Team=\"Home\">";
			string EndPattern = "</PlayerTricotNr>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string str(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			int tricotnumber = atoi(str.c_str());

			for (int i = 0; i < home->getNumberOfPlayerInTeam(); i++) {

				if (home->getPlayer(i)->getTrikotnumber() == tricotnumber) {
					setMember(home->getPlayer(i));
				}
			}
		}

		if (line.find("<PlayerTricotNr Team=\"Guest\">") != -1) {
			string StartPattern = "<PlayerTricotNr Team=\"Guest\">";
			string EndPattern = "</PlayerTricotNr>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string str(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			int tricotnumber = atoi(str.c_str());

			for (int i = 0; i < guest->getNumberOfPlayerInTeam(); i++) {

				if (guest->getPlayer(i)->getTrikotnumber() == tricotnumber) {
					setMember(guest->getPlayer(i));
				}
			}
		}

		if (line.find("<Card>") != -1) {
			string StartPattern = "<Card>";
			string EndPattern = "</Card>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string str(line.begin() + StartPos + sizePattern, line.begin() + EndPos);

			if (str == "yellow") {
				this->card = TCard::yellow;
			}
			if (str == "red") {
				this->card = TCard::red;
			}

		}

		if (line.find("</Foul>") != -1) {
			break;
		}
	}
}
