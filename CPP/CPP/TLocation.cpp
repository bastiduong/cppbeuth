#include "TLocation.h"



TLocation::TLocation()
{
}


TLocation::~TLocation()
{
}

void TLocation::load(ifstream &data) {
	string line = "";
	while (getline(data, line)) {

		if (line.find("<Name>") != -1)
		{
			string StartPattern = "<Name>";
			string EndPattern = "</Name>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string name(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			Stadium = name.c_str();
		}
		if (line.find("<Street>") != -1)
		{
			string StartPattern = "<Street>";
			string EndPattern = "</Street>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string name(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			setStreet(name.c_str());
		}
		if (line.find("<Postcode>") != -1)
		{
			string StartPattern = "<Postcode>";
			string EndPattern = "</Postcode>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string name(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			setPLZ(name.c_str());
		}
		if (line.find("<Town>") != -1)
		{
			string StartPattern = "<Town>";
			string EndPattern = "</Town>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string name(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			setCity(name.c_str());
		}
		if (line.find("<NumberOfSeats>") != -1)
		{
			string StartPattern = "<NumberOfSeats>";
			string EndPattern = "</NumberOfSeats>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string name(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			this->seats=atoi(name.c_str());
		}
		if (line.find("</Location>") != -1) {
			break;
		}
	}
}

