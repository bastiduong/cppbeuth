#include "TMember.h"


void TMember::print()
{
	printf("Mitglied Nr. %d\n", playerid);

	printf("%s; geb. ", name.c_str());
	birthday.printDate();
	printf("\n");

	adress.print();
}

void TMember::load(ifstream &data)
{
	string line = "";
	while (getline(data, line)) {

		if (line.find("<Name>") != -1) {
			string StartPattern = "<Name>";
			string EndPattern = "</Name>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string str(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			this->name = str.c_str();
		}

		if (line.find("<Birthday>") != -1) {
			TDate *_date = new TDate();
			_date->load(data);
			this->birthday = *_date;
		}

		if (line.find("<Address>") != -1) {
			TAddress *_adress = new TAddress();
			_adress->load(data);
			this->adress = *_adress;

		}

		if (line.find("<Team>") != -1) {
			string StartPattern = "<Team>";
			string EndPattern = "</Team>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string str(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			this->team = str.c_str();
		}

		if (line.find("<TricotNr>") != -1) {
			string StartPattern = "<TricotNr>";
			string EndPattern = "</TricotNr>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string str(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			this->setTrikotNumber(atoi(str.c_str()));
		}

		if (line.find("<MemberNr>") != -1) {
			string StartPattern = "<MemberNr>";
			string EndPattern = "</MemberNr>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string str(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			this->playerid = atoi(str.c_str());
		}

		if (line.find("</Trainer>") != -1 || line.find("</Member>") != -1 || line.find("</Player>") != -1) {
			break;
		}
	}
}
