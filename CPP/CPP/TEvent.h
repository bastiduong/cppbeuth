#ifndef TEVENT_H
#define TEVENT_H
#include <iostream>
#include <string>
#include "TMember.h"
#include "TTeam.h"

class TEvent
{
private:
	int gameMinute;
	TMember *member;

public:
	void setGameMinute(int min) {
		gameMinute = min;
	}

	int getGameMinute() {
		return gameMinute;
	}
	
	void setMember(TMember *_member) {
		member = _member;
	}

	TMember getMember() {
		return *member;
	}
	
	virtual void print() {};
	friend ostream &operator<< (ostream &ostr, TEvent &event);
	TEvent();
	~TEvent();
};

#endif
