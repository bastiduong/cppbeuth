#ifndef TADDRESS_H
#define TADDRESS_H
#include <fstream>
#include <iostream>
#include <strstream>
#include <string>
using namespace std;

class TAddress
{
public:
	TAddress() {};
	TAddress(string street, string plz, string city);
	~TAddress() {}
	void print();
	void load(ifstream &data);

	void setStreet(string street) { this->street = street; };
	void setPLZ(string plz) { this->plz = plz; };
	void setCity(string city) { this->city = city; };

	string getStreet() { return street; }
	string getPLZ() { return plz; }
	string getCity() { return city; }
private:
	string street;
	string plz;
	string city;
};


#endif