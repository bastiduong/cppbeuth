#include "TDateTime.h"



TDateTime::TDateTime()
{
}


TDateTime::~TDateTime()
{
}

void TDateTime::load(ifstream &filename) {
	string line = "";
	while (getline(filename, line)) {
		if (line.find("<Day>") != -1) {
			string StartPattern = "<Day>";
			string EndPattern = "</Day>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string name(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			setDay(atoi(name.c_str()));
		}
		if (line.find("<Month>") != -1) {
			string StartPattern = "<Month>";
			string EndPattern = "</Month>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string name(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			setMonth(atoi(name.c_str()));
		}
		if (line.find("<Year>") != -1) {
			string StartPattern = "<Year>";
			string EndPattern = "</Year>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string name(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			setYear(atoi(name.c_str()));
		}

		if (line.find("<Hour>") != -1) {
			string StartPattern = "<Hour>";
			string EndPattern = "</Hour>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string name(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			setHour(atoi(name.c_str()));
		}
		if (line.find("<Minute>") != -1) {
			string StartPattern = "<Minute>";
			string EndPattern = "</Minute>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string name(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			setMin(atoi(name.c_str()));
		}
	
		if (line.find("</DateTime>") != -1) {
			break;
		}
	}
}

void TDateTime::print()
{
	cout << this->getDay();
	cout << ".";
	cout << this->getMonth();
	cout << ".";
	cout << this->getYear();

	cout << ";";

	TTime::print();
}
