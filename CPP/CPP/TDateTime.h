#ifndef TDATETIME_H
#define TDATETIME_H

#include "TDate.h"
#include "TTime.h"

class TDateTime : public TDate , public TTime
{
public:
	TDateTime();
	~TDateTime();
	void load(ifstream &data);
	void print();
};

#endif
