#ifndef TMATCH_H
#define TMATCH_H

#include "TDateTime.h"
#include "TLocation.h"
#include "TTeam.h"
#include "TEvent.h"
#include <string>
#include <vector>
#include <iostream>
#include "TLeague.h"
#include "TList.h"

class TLeague;


class TMatch
{
private:
	TList <TEvent *> gameEvents;
	TDateTime start;
	TLocation location;
	TTeam *guest,*home;
	int guestPoints = 0;
	int homePoints = 0;

public:
	TMatch();
	~TMatch();
	void load(ifstream &data, TLeague * league);
	void print();
	void incGuestPoints() { guestPoints++; }
	void incHomePoints() { homePoints++; }
	TTeam getGuestTeam() { return *guest; }
	TTeam getHomeTeam() { return *home; }
	TDateTime getTime() { return start; }
	void printEvents();
	int getHomePoints() { return homePoints; }
	int getGuestPoints() { return guestPoints; }
};


#endif

