#include "TAddress.h"

TAddress::TAddress(string street, string plz, string city)
{
	this->street = street;
	this->plz = plz;
	this->city = city;
}

void TAddress::print()
{
	printf("%s; %s %s", street.c_str(), plz.c_str(), city.c_str());
}

void TAddress::load(ifstream &data)
{
	string line = "";

	while (getline(data, line)) {

		if (line.find("<Street>") != -1) {
			string StartPattern = "<Street>";
			string EndPattern = "</Street>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string str(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			this->street = str.c_str();
		}

		if (line.find("<Postcode>") != -1) {
			string StartPattern = "<Postcode>";
			string EndPattern = "</Postcode>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string str(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			this->plz = str.c_str();
		}

		if (line.find("<Town>") != -1) {
			string StartPattern = "<Town>";
			string EndPattern = "</Town>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string str(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			this->city = str.c_str();
		}

		if (line.find("</Address>") != -1) {
			break;
		}
	}
}

//void TAddress::SetAdress(TAddress _adress)
//{
//	this->street = _adress.getStreet();
//	this->plz = _adress.getPLZ();
//	this->city = _adress.getCity();
//}
