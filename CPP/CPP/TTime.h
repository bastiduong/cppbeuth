#ifndef TTIME_H
#define TTIME_H


class TTime
{
public:
	TTime(int hour, int minute, int seconds = 0) : hour(hour), minute(minute), seconds(seconds) {};
	TTime();
	void setTime(int hour, int minute, int seconds = 0);
	void setHour(int hour);
	void setMin(int min);
	void setSec(int sec);
	void print();
private:
	int hour;
	int minute;
	int seconds;
};

#endif
