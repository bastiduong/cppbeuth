#include "TTeam.h"

void TTeam::addMemberToTeam(TMember * player)
{
	listOfMemberOfTeam[currentPlayerInThisTeam] = player;
	currentPlayerInThisTeam++;
}

void TTeam::load(ifstream &data) {
	string line = "";
	while (getline(data, line)) {

		if (line.find("<Name>") != -1) {
			string StartPattern = "<Name>";
			string EndPattern = "</Name>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string str(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			this->teamname = str.c_str();
		}

		if (line.find("<Trainer>") != -1) {
			TMember *_member = new TMember();
			_member->load(data);
			this->trainer = *_member;

		}

		if (line.find("</Team>") != -1) {
			break;
		}
	}
}


