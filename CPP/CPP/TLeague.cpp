#include "TLeague.h"
#include "TSportsclub.h"
#include <string>
#include <vector>
#include <iostream>

TLeague::TLeague(string filename)
{
	load(filename);
}


TLeague::~TLeague()
{
}

void TLeague::load(string filename) {

	ifstream data;
	data.open(filename);
	string line;

	while (getline(data, line)) {

		if (line.find("<KindOfSports>") != -1) {
			string StartPattern = "<KindOfSports>";
			string EndPattern = "</KindOfSports>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string name(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			sport = name.c_str();
		}
		else if (line.find("<Name>") != -1) {
			string StartPattern = "<Name>";
			string EndPattern = "</Name>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string name(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			league = name.c_str();
		}
		else if (line.find("<Sportsclub>") != -1)
		{
			TSportsclub *_sportsclub = new TSportsclub();
			_sportsclub->load(data);
			club.push_back(*_sportsclub);

		}
		else if (line.find("<Match>") != -1)
		{
			TMatch *_match = new TMatch();
			_match->load(data, this);
			games.push_back(*_match);

		}
	}
}

void TLeague::print() {
	//Test print
	cout << league <<" "<< sport << endl;
	cout << endl;
	cout << "Vereine und deren Mannschaften:" << endl;

	for each (TSportsclub var in club)
	{
		cout << "-" << var.getClubname() << endl;
		for (int i = 0; i < var.getCurrentNumberOfTeams(); i++) {
			cout << "   >" << var.getTeam(i)->getTeamname() << endl;
		}
	}

	cout << "Spiele:" << endl;

	for each (TMatch var in games)
	{
		cout << "-" << var.getHomeTeam().getTeamname() << " gegen " << var.getGuestTeam().getTeamname() << endl;
		cout << "   Datum / Uhrzeit: ";
		var.getTime().print();
		cout << endl;
		var.printEvents();

		cout << "Endstand " << var.getHomePoints() << " (Heim) : " << var.getGuestPoints() << " (Gast)" << endl;
	}
}

TTeam *TLeague::getTeamByName(string name)
{
	TTeam *team = new TTeam();
	for each (TSportsclub var in club)
	{
		for (int i = 0; i < var.getCurrentNumberOfTeams(); i++) {
			
			if (var.getTeam(i)->getTeamname() == name) {
				team = var.getTeam(i);
			}
		}
	
	}
	return team;
}
