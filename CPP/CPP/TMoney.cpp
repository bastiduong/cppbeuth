#include "TMoney.h"
#include <cstdio>
#include <iostream>

using namespace std;

TMoney::TMoney()
{
	money = 0;
	currency = "EUR";
}

TMoney::TMoney(double money, string currency)
{
	this->money = money;
	this->currency = currency;
}

void TMoney::SetValue(double money)
{
	this->money = money;
}

void TMoney::SetCurrency(string currency)
{
	this->currency = currency;
}

void TMoney::print()
{
	printf("%.2f %s", money, currency.c_str());
}
