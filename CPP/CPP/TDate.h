#define _CRT_SECURE_NO_WARNINGS 1
#ifndef TDATE_H
#define TDATE_H
#include <fstream>
#include <iostream>
#include <strstream>
#include <string>

using namespace std;

class TDate
{
public:
	TDate();
	TDate(int day, int month, int year);
	~TDate() {}
	void SetDate(int day, int month, int year);
	int getDay() { return day; }
	int getMonth() { return month; }
	int getYear() { return year; }
	friend ostream &operator<< (ostream &ostr, TDate &date);
	void setDay(int day);
	void setMonth(int month);
	void setYear(int year);
	void printDate();
	void load(ifstream &data);

private:
	int day;
	int month;
	int year;
};



#endif // !TDATE_H
