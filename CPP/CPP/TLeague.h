#ifndef TLEAGUE_H
#define TLEAGUE_H
#include <string>
#include <vector>
#include <iostream>
#include "TSportsclub.h"
#include "TMatch.h"
#include "TList.h"


using namespace std;
class TMatch;


class TLeague
{
private:
	string league , sport;
	TList<TSportsclub> club;
	TList <TMatch> games;


public:

	TLeague(string filename);
	~TLeague();
	void load(string filename);
	void print();
	TTeam *getTeamByName(string name);
};

#endif