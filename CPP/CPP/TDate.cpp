#include "TDate.h"
#include <ctime>
#include <cstdio>

using namespace std;

TDate::TDate() {
	time_t currentTime;
	tm *localTime;
	time(&currentTime);
	localTime = localtime(&currentTime);

	int Day = localTime->tm_mday;
	int Month = localTime->tm_mon + 1;
	int Year = localTime->tm_year + 1900;
	
	SetDate(Day, Month, Year);

}

TDate::TDate(int day, int month, int year) {
	SetDate(day, month, year);
}

void TDate::setDay(int day) {
	this->day = day;
}

void TDate::setMonth(int month) {
	this->month = month;
}

void TDate::setYear(int year) {
	this->year = year;
}

void TDate::printDate()
{
	printf("%02d.%02d.%04d", day, month, year);
}

void TDate::load(ifstream &data)
{
	string line = "";
	while (getline(data, line)) {

		if (line.find("<Day>") != -1) {
			string StartPattern = "<Day>";
			string EndPattern = "</Day>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string str(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			this->day = atoi(str.c_str());
		}

		if (line.find("<Month>") != -1) {
			string StartPattern = "<Month>";
			string EndPattern = "</Month>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string str(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			this->month = atoi(str.c_str());
		}

		if (line.find("<Year>") != -1) {
			string StartPattern = "<Year>";
			string EndPattern = "</Year>";
			int StartPos = (int)line.find(StartPattern);
			int EndPos = (int)line.find(EndPattern);
			int sizePattern = (int)StartPattern.size();
			string str(line.begin() + StartPos + sizePattern, line.begin() + EndPos);
			this->year = atoi(str.c_str());
		}
		if (line.find("</Establishment>") != -1 || line.find("</Birthday>") != -1) {
			break;
		}

	}
}


void TDate::SetDate(int day, int month, int year) {
	setDay(day);
	setMonth(month);
	setYear(year);
}

ostream & operator<<(ostream & ostr, TDate & date)
{
	date.printDate();
	return ostr;
}
